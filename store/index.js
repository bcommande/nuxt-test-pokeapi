export const state = () => ({
    data: [],
    allData: [],
    filter: {
        id: 2,
        name: 0,
        s: 0
    }
})

export const mutations = {
    SET_DATA_POKEMON(state, dataPokemon) {
        state.data = dataPokemon
    },
    SET_ALL_DATA_POKEMON(state, dataPokemon) {
        state.allData = dataPokemon
    },
    SET_DATA_RESULTS(state, dataPokemon) {
        state.data.results = dataPokemon
    },
    SET_SEARCH_FILTER(state, s) {
        state.filter.s = s
    },
    SET_ID_FILTER(state, id) {
        state.filter.id = id
    },
    ORDER_BY_ID(state, filter){
        let data = state.data.results;
        data.sort((a, b) => a.id - b.id)
        state.filter.name = 0
        if (filter == 2){
            data = data.reverse()
            state.filter.id = 1
        }
        else{
            state.filter.id = 2
        }

    },
    ORDER_BY_NAME(state, filter){
        let data = state.data.results;
        data.sort((a, b) => a.name.localeCompare(b.name))
        state.filter.id = 0
        if (filter == 2){
            data = data.reverse()
            state.filter.name = 1
        }
        else{
            state.filter.name = 2
        }
       
    }
}

export const actions = {
    async loadPokemon({ commit }) {
        let response = await this.$axios.$get(`https://pokeapi.co/api/v2/pokemon?limit=25`)
        response.page = 1
        deserializeData(response.results)
        commit('SET_DATA_POKEMON', response)
        commit('SET_ALL_DATA_POKEMON', response.results)
    },

    async loadMore({ commit }, link) {
        if (this.state.data.page > 6) {
            return false
        }
        let response = await this.$axios.get(link)
        response = response.data
        response.page = this.state.data.page + 1
        deserializeData(response.results)
        commit('SET_DATA_POKEMON', response)
        commit('SET_ALL_DATA_POKEMON', response.results)
    },

    search({ commit }, s) {
        if (s == "") {
            data = this.state.allData.results
            commit('SET_SEARCH_FILTER', 0)
            commit('SET_DATA_RESULTS', data)
        }
        let data = filtreTexte(this.state.allData, s)
        commit('SET_DATA_RESULTS', data)
        commit('SET_SEARCH_FILTER', 1)

    },
    orderById({ commit }, {value, load}) {
        commit('ORDER_BY_ID', value)
    },
    orderByName({commit}, {value, load}){
        commit('ORDER_BY_NAME', value)
    }
}

const deserializeData = function (data) {
    data.forEach(element => {
        element.id = element.url.split('/')[6]
    });
}
const filtreTexte = (arr, requete) => {
    return arr.filter(el => el.name.toLowerCase().indexOf(requete.toLowerCase()) !== -1);
}