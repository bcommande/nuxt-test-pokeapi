import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueMeta from 'vue-meta'
import index from '@/pages/index.vue'

const localVue = createLocalVue()
localVue.use(VueMeta, { keyName: 'head' })

describe('index.vue', ()=>{
  let wrapper;
  beforeEach(()=>{
    wrapper = shallowMount(index, {
      localVue
    })
  })

  afterEach(()=>{
    if (wrapper) {
      wrapper.destroy()
    }
    })

  it('has correct <head> content', ()=>{
    expect(wrapper.vm.$metaInfo.title).toBe('Home')
    const descriptionMeta = wrapper.vm.$metaInfo.meta.find(
      (item) => item.hid === 'home'
    )
    expect(descriptionMeta.content).toBe('List of pokemons')
  })

  it('renders',()=>{
    expect(wrapper.exists()).toBe(true)
  })

  it('has h1',()=>{
    expect(wrapper.find('h1').text()).toBe('Pokédex')
  })
})
